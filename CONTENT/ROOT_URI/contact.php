<link rel="canonical" href="https://teenybeans.in/contact" />
<title>Contact Us to open a preschool | Teeny Beans</title>
<meta name="description" content="The best preschool solution in India by Teeny Beans. Contact us to open your own branded Preschool without royalty fees. ">

<style>
       /* Set the size of the div element that contains the map */
  #map {
    height: 400px;  /* The height is 400 pixels */
    width: 100%;  /* The width is the width of the web page */
   }
   .slide.kenburns{
			background-image:url('/images/web/5.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/web/5-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!-- <section id="page-title" data-bg-parallax="images/parallax/5.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Contact Us</h1>
			<span>Feel free to tell your needs!</span>
		</div>
	</div>
</section> -->

<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<!-- <div class="slide-captions text-center text-light"> -->
				<div class="page-title text-center text-light">
					<h1>Contact Us</h1>
					<span>Feel free to tell your needs!</span>
				</div>
			</div>
	</div>
</section>

<section>
<div class="container">
<div class="row">
<div class="col-lg-6" style="background-image: url('/images/web/world-map-dark.png'); background-position: 25% 50px; background-repeat: no-repeat;">
<h3 class="text-uppercase">Get In Touch</h3>
	<div class="row m-t-40">
		<ul class="list-icon">
			<li><i class="fa fa-map-marker-alt"></i> DE 2B, VIP Rd, Desh Bandhu Nagar, Baguiati, Rajarhat, West Bengal 700059</li>
			<li><i class="fa fa-phone"></i> +91 97483 86431</li>
			<li><i class="far fa-envelope"></i> <a href="#">enquiry@beanstalkedu.com</a> </li>
			<li>
			<i class="far fa-clock"></i>Monday - Saturday: <strong>09:00 - 18:00</strong>
			Sunday: <strong>Closed</strong> </li>
		</ul>
	</div>
	<div class="social-icons m-t-30 social-icons-colored">
		<ul>
			<li class="social-facebook"><a href="https://www.facebook.com/teenybeanspreschoolcurriculum/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-youtube"><a href="https://www.youtube.com/channel/UC4ppJKZgGaOxexol-zMwZDQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
            <li class="social-twitter"><a href="https://twitter.com/teeny_beans" target="_blank"><i class="fab fa-twitter"></i></a></li>
            <li class="social-instagram"><a href="https://www.instagram.com/teenybeans.in/" target="_blank"><i class="fab fa-instagram"></i></a></li>
		</ul>
	</div>
</div>
<div class="col-lg-6">
	<div class="alert alert-danger" id="warning" style="display: none;">Fill all the fields!</div>
    <div class="alert alert-success" id="success" style="display: none;">Thank You! We will contact you soon!</div>
	<form class="" role="form" method="post">
		<div class="row">
			<div class="form-group col-md-6">
				<label for="name">Name</label>
				<input type="text" aria-required="true" name="widget-contact-form-name" required class="form-control required name" id="name1" placeholder="Enter your Name">
			</div>
			<div class="form-group col-md-6">
				<label for="email">Email</label>
				<input type="email" id="email" aria-required="true" name="widget-contact-form-email" required class="form-control required email" placeholder="Enter your Email">
				<p id="emailError" style="color: red; display: none;">Enter a valid email address</p>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="telephone">Contact No</label>
				<input class="form-control" id="phn" type="number" name="telephone" placeholder="Enter your Contact number" required>
			</div>
			<div class="form-group col-md-6">
				<label for="addr">Address</label>
				<input type="text" class="form-control" id="addr" name="address" placeholder="Enter your Full Address" required>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="gender">What is the nature of your request?</label>
				<select id="req" class="form-control" name="gender" required>
					<option value="">Select your requirements</option>
					<option>New Preschool Setup</option>
					<option>Existing preschool Upgrade</option>
					<option>Preschool setup for High School</option>
					<option>Others</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="message">Message</label>
			<textarea type="text" name="widget-contact-form-message" required rows="5" class="form-control required" id="message" placeholder="Enter your Message"></textarea>
		</div>

		<button class="btn" type="button" onclick="submitForm()"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
	</form>
</div>
</div>
</div>
</section>


<section class="no-padding">

<div id="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14732.289409870642!2d88.4296203!3d22.6137732!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2dc64c288b04969!2sTeeny%20Beans!5e0!3m2!1sen!2sin!4v1587021587217!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" loading="lazy"></iframe>
</div>

<!-- <div class="map" data-latitude="22.615367" data-longitude="88.430383" data-style="light" data-info="Hello from Teeny Beans"></div> -->

</section>
<script type="text/javascript">
    var option= null;
    var name= null;
    var addr= null;
    var phn= null;
    var subject = null;
    var email= null;
    var message= null;
    var warning = document.getElementById('warning');
    var success = document.getElementById('success');
    var emailError = document.getElementById('emailError');
    var req = document.getElementById("req");
    function setOptions(s) {
      // console.log(s[s.selectedIndex].value); // get value
      option = s[s.selectedIndex].value;
      // console.log(s[s.selectedIndex].id); // get id
    }
    function submitForm(){
    	name = document.getElementById("name1").value;
	    phn = document.getElementById("phn").value;
	    addr = document.getElementById("addr").value;
	    email = document.getElementById("email").value;
	    message = document.getElementById("message").value;
    	success.style.display = "none";
        subject = req.options[req.selectedIndex].text;
        // console.log(name+phn+addr+email+message);
        if(name == '' || addr=='' || phn == '' || email == '' || message==''){
            warning.style.display = "block";
        }else{
        	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

	        if (reg.test(email) == false) 
	        {
	            emailError.style.display = "block";
	        }else{
	        	emailError.style.display = "none";
	        	 warning.style.display = "none";
	            let formData = new FormData();
	            formData.append('formName', 'teenyBeansNewContact');
	            formData.append('Name', name);
	            formData.append('Email', email);
	            formData.append('Phone', phn);
	            // formData.append('Address', addr);
	            formData.append('MessageDetails', message+" Subject= "+subject);
	            fetch('https://api.teenybeans.in/API/contactFormProcessor/v1/', {
	              method: 'POST',
	              body: formData
	            })
	            .then(res => res.json())
	            .then(json =>  {
	              // console.log(json);
	              // sendMail("enquiry@atheneumglobal.education");
	              sendMail("enquiry@beanstalkedu.com");
	              sendMail("teenybeans.info@gmail.com");
	              // sendMail("chatterjeegouravking@gmail.com");
	              sendWelcomeMail(email);
	              success.style.display = "block";
	              document.getElementById("name1").value="";
	              document.getElementById("phn").value="";
	              document.getElementById("addr").value="";
	              document.getElementById("email").value="";
	              document.getElementById("message").value="";

	              // window.location.href = "contact-back";
	              }
	            );
	        }
           
        }
        // if(name)
    }

     function sendMail(sendEmail) {
      // ---------Mail sent-----------------
      
      let formData = new FormData();
      formData.append("sendMail", "true");
      formData.append("reciever", sendEmail);
      formData.append("sender", "Teenybeans");
      formData.append("senderMail", "no-reply@teenybeans.in");
      formData.append("subject", "New Contact Form Fillup");
      formData.append(
        "message",
        "<html><body><p>New Contact form is filled up.</p><br><p>User Details:-</p><table><tr><th>Name:- </th><td>:- " +
          name +
          "</td></tr><tr><th>Email:- </th><td>:- " +
          email +
          "</td></tr><tr><th>Phone:- </th><td>:- " +
          phn +
          "</td></tr><tr><th>Message:- </th><td>:- " +
          message +
          "</td></tr></table></body></html>"
      );
      fetch("https://mailapi.teenybeans.in/", {
        method: "POST",
        body: formData
      })
        .then(function(response) {
          response.json().then(function(data) {
            // console.log(data);
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
    }
    function sendWelcomeMail(sendEmail) {
      // ---------Mail sent-----------------
      console.log(sendEmail);
      let formData = new FormData();
      formData.append("sendMail", "true");
      formData.append("reciever", sendEmail);
      formData.append("sender", "Teenybeans");
      formData.append("senderMail", "no-reply@teenybeans.in");
      formData.append("subject", "Welcome to Teenybeans Preschool");
      formData.append(
        "message",
        "<html><body><p>Welcome " +
          name +
          " to Teenybeans Preschool.<br>Thank you for contacting us. <br> We will get back to you shortly. In the meanwhile please do check out our website for more information. click <a href='https://teenybeans.in'>here</a><br>Thank you<br>-Team Teenybeans. </p></body></html>"
      );
      fetch("https://mailapi.teenybeans.in/", {
        method: "POST",
        body: formData
      })
        .then(function(response) {
          response.json().then(function(data) {
            console.log(data);
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
    }
    
</script>

