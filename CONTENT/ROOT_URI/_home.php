<link rel="canonical" href="https://teenybeans.in" async/>
<title>Non Franchise Preschool | No Royalty Play School</title>
<meta name="description" content="Best low investment non-franchise preschool in India. Teeny Beans offers an affordable preschool, afterschool and teacher training solution without royalty fee.">
<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://teenybeans.in">
<meta property="og:title" content="Setup OWN Branded preschool">
<meta property="og:description" content="Preschool, afterschool, and teacher training solution without royalty fee.">
<meta property="og:image" content="https://teenybeans.in/images/web/og-image.jpg">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://teenybeans.in">
<meta property="twitter:title" content="Setup OWN Branded preschool">
<meta property="twitter:description" content="Preschool, afterschool, and teacher training solution without royalty fee.">
<meta property="twitter:image" content="https://teenybeans.in/images/web/og-image.jpg">
<style type="text/css">
  .wrapper {
  
  }

  .youtube {
    background-color: #000;
    margin-bottom: 30px;
    position: relative;
    padding-top: 56.25%;
    overflow: hidden;
    cursor: pointer;
  }
  .youtube img {
    width: 100%;
    top: -16.82%;
    left: 0;
    opacity: 0.7;
  }
  .youtube .play-button {
    width: 90px;
    height: 60px;
    background-color: #333;
    box-shadow: 0 0 30px rgba( 0,0,0,0.6 );
    z-index: 1;
    opacity: 0.8;
    border-radius: 6px;
  }
  .youtube .play-button:before {
    content: "";
    border-style: solid;
    border-width: 15px 0 15px 26.0px;
    border-color: transparent transparent transparent #fff;
  }
  .youtube img,
  .youtube .play-button {
    cursor: pointer;
  }
  .youtube img,
  .youtube iframe,
  .youtube .play-button,
  .youtube .play-button:before {
    position: absolute;
  }
  .youtube .play-button,
  .youtube .play-button:before {
    top: 50%;
    left: 50%;
    transform: translate3d( -50%, -50%, 0 );
  }
  .youtube iframe {
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
  }
  .heading-text.heading-section h1:before {
    content: "";
    position: absolute;
    height: 2px;
    width: 100px;
    background-color: #86bc42;
    bottom: -30px;
    left: 0;
    right: 0;
}
</style>
<?php include("_menu.php");?>


<!--- slider section start -->
<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">

  <div class="slide kenburns" id="slide0">
    <div class="bg-overlay"></div>
      <div class="container">
      <div class="slide-captions text-center text-light">
        <span class="strong">Re-launch your preschool online with Teeny Beans Digital Preschool Solution – 100% FREE</span>
        <h2 class="text-dark">Preschool @ Home Solution</h2>
        <a class="btn btn-light" href="/preschool-at-home">Know More</a>
      </div>
    </div>
  </div>

  <div class="slide kenburns" id="slide1">
    <div class="bg-overlay"></div>
      <div class="container">
      <div class="slide-captions text-center text-light">


        <span class="strong">Own your preschool brand. The most cost-effective and long term preschool solution. Operating breakeven in 13 months flat!</span>
        <h2 class="text-dark">Zero Royalty | Non-Franchise <br>Unlimited Tenure</h2>
        <a class="btn btn-light" href="/contact">Contact Now</a>

      </div>
    </div>
  </div>
  <div class="slide kenburns" id="slide2" style="">
    <div class="bg-overlay"></div>
      <div class="container">
      <div class="slide-captions text-center text-light">

        <span class="strong">International recognitions and affiliations. Designed in accordance with the national curriculum early childhood education framework, ministry of WCD, GOI.</span>
        <h2 class="text-dark">International Preschool Education</h2>

        <a class="btn btn-light" href="/contact">Contact Now</a>

      </div>
    </div>
  </div>
  <div class="slide kenburns" id="slide3" style="">
    <div class="bg-overlay"></div>
      <div class="container">
      <div class="slide-captions text-center text-light">

        <span class="strong">Internationally recognized teacher education programs international institute of Montessori teacher training. Pan-India network of teacher training institutes.</span>
        <h2 class="text-dark">International Teacher Education</h2>

        <a class="btn btn-light" href="/contact">Contact Now</a>

      </div>
    </div>
  </div>
  <div class="slide kenburns" id="slide4" style="">
    <div class="bg-overlay"></div>
      <div class="container">
      <div class="slide-captions text-center text-light">

        <span class="strong">Beanstalk 3P, Writo, MaxBrain Abacus, Super Phonics & Cambridge Young Learners English- Programs for all ages and seamlessly integrated with the preschool program.</span>
        <h2 class="text-dark">After School Activity Centre</h2>

        <a class="btn btn-light" href="/contact">Contact Now</a>

      </div>
    </div>
  </div>

</div>
<!--- slider section end -->


<!--- about section start -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="heading-text heading-section">
          <!-- <h2>Non Franchise Preschool</h2> -->
          <h1 style="font-size: 48px;">Non Franchise Preschool</h1>
        </div>
      </div>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-6">Teeny Beans is a non-franchise and zero-royalty based holistic solution for new and existing practitioners in the space of early childhood education. We help set up and operate international early learning centers that are globally benchmarked centres of excellence.</div>
          <div class="col-lg-6">In doing so we create institutions that adopt the highest standards in early childhood education and care and evolve into becoming experts in the field of early childhood education catering to local communities and preparing global citizens for 2050 and beyond.</div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--- about section end -->

<!--- video section start -->
<section class="background-grey">
  <div class="container">

    <div class="row">
      <div class="col-lg-6">
        <h2>NEW PRESCHOOL</h2>
        <div class="wrapper">
          <div class="youtube" data-embed="eEGaiRGUxgM">
            <div class="play-button"></div>
          </div>
        </div>
        <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/eEGaiRGUxgM?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
      </div>
      <div class="col-lg-6">
        <h2>EXISTING PRESCHOOL</h2>
        <div class="wrapper">
          <div class="youtube" data-embed="1vc-WFWJUCE">
            <div class="play-button"></div>
          </div>
        </div>
        <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/1vc-WFWJUCE?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
      </div>
    </div>
  </div>
</section>
<!--- video section end -->

<!-- award section start --->
<section>
  <div class="container">
    <div class="row">
        <div class="col-md-4">
        <picture>
  <source srcset="
  /images/web/Best-Preschool-Franchise-siliconindia-Magazine.webp" type="image/webp">
  <source srcset="
  /images/web/Best-Preschool-Franchise-siliconindia-Magazine.jpg" type="image/jpeg">
    <img src="/images/web/Best-Preschool-Franchise-siliconindia-Magazine.jpg" alt="Best Preschool franchise award from Silicon India" style="width:70%;" loading="lazy">
</picture>  
        </div>
        <div class="col-md-8">
          <p style="font-size: 26px; line-height: 35px; font-style: italic;">
            As seen on Silicon India Magazine - on being selected as <strong>‘Most Promising Pre School Franchise 2020’</strong>
          </p>
          <p>
            <a href="https://beanstalkedu.com/blog/an-interview-with-silicon-india-magazine" rel="dofollow"><strong>Read the Full Story</strong></a>
          </p>
        </div>
      </div>
    </div>
</section>
<!-- award section end --->

<!-- call to action section start --->
<!-- <section>
  <div style="background-image:url(images/parallax/7.jpg)" class="call-to-action background-image mb-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-10">
          <h3 class="text-light">
          Join by April 27 and <span>Win $200</span> in Programs and Services
          </h3>
          <p class="text-light">
          This is a simple hero unit, a simple call-to-action-style component for calling extra attention to featured content.
          </p>
          </div>
          <div class="col-lg-2">
          <a class="btn btn-light btn-outline">Call us now!</a>
        </div>
      </div>
    </div>
  </div>
</section> -->
<!-- call to action section end --->

<!--- testimonials section start -->
<section style="background-image:url('/images/web/general-bg.jpg');">
  <div class="container">
    <div class="heading-text text-center text-light">
      <h2>What people are saying!</h2>
    </div>

    <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left text-light" data-items="1">
      <div class="testimonial-item">
        <img src="/images/web/stepping-stones-ip-owner.jpeg" alt="Stepping Stones IP Owner" loading="lazy">
        <p>I'm very thankful of teeny beans, why because when I plan to open a preschool in Nagpur I was very confused about how to start and what to do and things were going very messy then I started searching for a partner but which ever company or school I approach, they were asking for deposit, royalty and many more but when I came across teeny beans I was totally satisfy of no royalty, no deposit no franchise nothing and given fully support as I got  a complete preschool.</p>
        <span>Sudha Mangesh Gosavi </span>
        <span>Director of Steeping Stones International Preschool</span>
      </div>

      <div class="testimonial-item">
          <img src="/images/web/nest-ip-owner.jpeg" alt="Nest IP Owner" loading="lazy">
        <p>Being a beginner in this business, I was a bit tensed to open up the PRE-SCHOOL...  But the continuous support rather guidance regarding the whole training process,  benefits,  selection of area,  pre advertisement , etc. make me confident and satisfied to start with it.</p>
        <span>Sharmistha Misra</span>
        <span>Director of NEST International Preschool </span>
      </div>

    </div>

  </div>
</section>
<!--- testimonials section end -->

<!-- faq section -->
<section>
  <div class="container">
    <div class="row">
        <div class="col-md-4">
        <picture>      
          <source srcset="
          /images/web/child-development.webp" type="image/webp">
          <source srcset="
          /images/web/child-development.jpg" type="image/jpeg">
          <img src="/images/web/child-development.webp" alt="best preschool franchise without royalty" class="img-responsive" loading="lazy">
        </picture>  
          
        </div>
        <div class="col-md-8">
          <h2>Some FAQ</h2>
          <div class="accordion accordion-shadow">
            <div class="ac-item">
              <h5 class="ac-title">What is the solution proposed by Teeny Beans?</h5>
              <div class="ac-content">
                  <p>At Teeny Beans, we help co-create individually branded preschools & early childhood learning centres of excellence.</p>
                  <p>
                      What sets us apart from any other solution provider is the fact that our solution equips individuals to set up and operate a highly diversified business with multiple value streams, a solution which we call
                      <strong>integrated learning centre</strong>.<br />
                      <br />
                      Specifically, an integrated learning centre is characterized by
                  </p>
                  <ul class="list-icon list-icon-check">
                      <li>An international preschool</li>
                      <li>An international teacher training institute</li>
                      <li>A kids afterschool activity centre</li>
                  </ul>
                  <p>
                      Our solution is a holistic business solution for our partners and not a piecemeal solution. And most importantly our solution is a<strong>non-franchise zero royalty model<strong> </strong></strong>the only model we believe is
                      relevant in the preschool space.
                  </p>
              </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">How is Teeny Beans different from a preschool franchisor ?</h5>
                <div class="ac-content">
                  <p>For starters, no discussion post set up is ever going to be about commercials or royalties.
                    It’s only going to be about the curriculum, delivery, training, recruitments, parental concerns, etc.
                    Things that assume only peripheral significance for any preschool franchisor. This would resonate strongly with existing preschool franchise owners.</p>
                  <p>We offer a one-time investment with zero royalties and commissions. We co-implement the curriculum with you and co-administer the ILC along with you – forever. All the while, the attempt would be to make your brand resonate amongst your local community through strong marketing and rigorous implementation of a fantastic curriculum.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">Do you charge royalty for your services ?</h5>
                <div class="ac-content">
                  <p>We do not charge any royalty for setting up a preschool. We operate on a non-franchise, Zero Royalty mode. We help you build an everlasting individual branded ILC and expect we’d be doing enough to help you become the early learning expert practitioner that you had wanted to be.</p>
                </div>
            </div>
            <div class="ac-item">
              <h5 class="ac-title">What Training does Teeny Beans provide ?</h5>
              <div class="ac-content">
                  <p>
                      Training is a continuous process. This is why we would be the only solution provider in this space that does not sever ties post-sales but in fact increases engagements once your school is up and running. Following are the
                      touchpoints for training -
                  </p>
                  <ul class="list-icon list-icon-check">
                      <li>We provide a 3-day in-person intensive training in Kolkata in our model preschool Beanstalk International Preschool.</li>
                      <li>Preschool owners will also have a one-one skype session with our Co-Founder to understand business basics and his own professional journey in establishing a successful ILC.</li>
                      <li>We also have an e-learning module on the various aspects of the preschool set up and curriculum training that augments one's understanding.</li>
                      <li>Every new teacher or coordinator joining the preschool goes through the same e-learning module on curriculum training.</li>
                  </ul>
                </div>
            </div>
          </div>
          <p>
            <a href="/faq" class="btn mt-5">Read Full FAQs</a>
          </p>
        </div>
      </div>
    </div>
</section>

<!--- Blog section start -->
<section class="content background-grey">
  <div class="container">
    <div class="heading-text heading-section">
      <h2>OUR BLOG</h2>
      <!---<span class="lead"></span> --->
    </div>
    <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
      <?php 
          $sql = "SELECT * FROM PAGES WHERE `DOMAIN` = 'teenybeanspreschoolcurriculum.com' ORDER BY PAGE_ID DESC LIMIT 3";
          $result = mysqli_query($link, $sql);
          if(mysqli_num_rows($result)>0){
            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
              $id = $row['PAGE_ID'];
              $domain = $row['DOMAIN'];
              $uri = $row['URI'];
              $slug = $row['SLUG'];
              $title = $row['TITLE'];

              echo '<div class="post-item border">
                <div class="post-item-wrap">
                  <div class="post-image">
                    <a href="'.$slug.'">
                      <img alt="'.$title.'" src="https://bscdn.sgp1.digitaloceanspaces.com/publicImages/'.$domain.$uri.$slug.'.jpg" loading="lazy">
                    </a>
                  </div>
                  <div class="post-item-description">
                    <h2>
                      <a href="'.$slug.'">'.$title.'</a>
                    </h2>
                    <p>For starters, no discussion post set up is ever going to be about commercials or royalties.</p>
                    <a href="'.$slug.'" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>';

                // echo '<div class="col-sm-4 col-md-6 col-lg-2">
                // <a href="singleProduct?id='.$id.'"><img style="margin-bottom:5px; border: 2px solid;" src="/CONTENT/UPLOADS/PRODUCT/'.$id.'/'.$imgName.'"  width="90px" height="90px"></a>
                // </div>';
             }
          }else{
            // echo mysqli_error($link);
            echo '<div class="alert alert warning">Blog Posts are coming!</div>';
          }

        ?>


    </div>
  </div>
</section>
<!--- Blog section end -->


<!--- long text section start -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="heading-text heading-section">
        <h2>Preschool setup without any Royalty Fee</h2>
        <p class="lead">The larger the franchisor recall, the bigger the franchisee fee and set up cost. So, how useful is this? Well, in one line you're paying for advertising their brand while you can establish your own brand with much less investment.</p>
      </div>

      <h3>Best alternative to a low investment preschool franchise</h3>
        <div class="col-lg-12">
          <p>We drive down your investment to the bare minimum by focusing your resources on core requirements.</p>
        <picture>      
          <source srcset="
          /images/web/kids-preschool.webp" type="image/webp">
          <source srcset="
          /images/web/kids-preschool.jpg" type="image/jpeg">
          <img alt="preschool kid playing" src="/images/web/kids-preschool.webp" style="width:50%; float: left;" loading="lazy">
        </picture> 
          
          <p>Make no mistake. We don''t help you set up a cheap preschool! We help you to utilize your resources wisely to build an efficient preschool set up. How much you spend inside the preschool in non-core requirements is your business decision.</p>


          <p>We provide you with an evaluation framework for your investment suggesting ideal investments considering the demographic profile of consumers in your locality. We equip you with the know-how, equipment, learning aids, curriculum, and architectural designs to start off with a preschool without any royalty fees.</p>


          <p>We help you with every aspect of brand building that established national preschool franchisors provide. And be rest assured, these efforts, which straddles online and offline mediums, help you establish YOUR brand in your local community. You create an identity that outlives any franchisee term that any franchisor provides you without paying the franchise fee.</p>
          <p>When you haven't started a preschool it's a little difficult to comprehend the advantages conveyed in the above statement, but every preschool franchisee who has operated with a preschool franchisor would know the value of that statement. If you have to spend your time, money and effort in building an institution like a preschool let that institution be YOUR OWN. That's what Teeny Beans does for you.</p>
          <p>Teeny Beans non-franchise and no royalty preschool model for opening a playschool in India is a highly successful model that places the partner at the heart of all considerations. We help you create an institution that will outlast franchisee tenures, abet wealth creation and create an everlasting institution that only YOU own. Your Preschool, Your Brand. We invite you to find out more about Teeny Beans, the Brand behind Brands.</p>
        </div>
       </div>
  </div>
</section>
<!--- long text section end -->



